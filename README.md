# Web Origin

## Posts

- [A Basic Glance at the Virtual Table](http://blog.httrack.com/blog/2014/05/09/a-basic-glance-at-the-virtual-table/) - typical C++ object memory layout
- [How long does it take to make a context switch?](http://blog.tsunanet.net/2010/11/how-long-does-it-take-to-make-context.html) - performance profiling for Linux context switch
- [A Whirlwind Tutorial on Creating Really Teensy ELF Executables for Linux](http://www.muppetlabs.com/~breadbox/software/tiny/teensy.html) - Linux ELF structure
- [The TTY demystified](http://www.linusakesson.net/programming/tty/index.php) - UNIX tty, pts, shell, job control explained
- [Inside a super fast CSS engine: Quantum CSS (aka Stylo)](https://hacks.mozilla.org/2017/08/inside-a-super-fast-css-engine-quantum-css-aka-stylo/)
- [The whole web at maximum FPS: How WebRender gets rid of jank](https://hacks.mozilla.org/2017/10/the-whole-web-at-maximum-fps-how-webrender-gets-rid-of-jank/)
- [The Illustrated TLS Connection](https://tls.ulfheim.net/) - every byte of a TLS 1.2 connection explained and reproduced
- [Cameras and Lenses](https://ciechanow.ski/cameras-and-lenses/) - datailed illustration of cameras and lenses
- [Getting the World Record in HATETRIS](https://hallofdreams.org/posts/hatetris/) - an optimization of HATETRIS in Rust
- [What is ChatGPT doing ... and Why Does It Work](https://writings.stephenwolfram.com/2023/02/what-is-chatgpt-doing-and-why-does-it-work/) - ChatGPT explained

## Books

- [Forecasting: Principles and Practice](https://otexts.com/fpp/) - by Rob J Hyndman and George Athanasopoulos
- [Interactive SICP](https://xuanji.appspot.com/isicp/) - Interactive version of `Structure and Interpretation of Computer Programs`
